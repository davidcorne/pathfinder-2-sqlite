ability_mods:
  cha_mod: 5
  con_mod: 4
  dex_mod: 3
  int_mod: 3
  str_mod: 7
  wis_mod: 4
ac: 37
ac_special: null
alignment: LG
automatic_abilities:
- action_cost: None
  critical_failure: The creature is frightened 4.
  critical_success: The creature is unaffected by the presence.
  description: 90 feet, DC 33
  effect: null
  effects: null
  failure: The creature is frightened 2.
  frequency: null
  full_description: null
  generic_description: A creature that first enters the area must attempt a Will save.
    Regardless of the result of the saving throw, the creature is temporarily immune
    to this monster's Frightful Presence for 1 minute.
  name: Frightful Presence
  range: null
  raw_description: '**Frightful Presence** 90 feet, DC 33 A creature that first enters
    the area must attempt a Will save. Regardless of the result of the saving throw,
    the creature is temporarily immune to this monster''s Frightful Presence for 1
    minute.

    Critical Success The creature is unaffected by the presence.

    Success The creature is frightened 1.

    Failure The creature is frightened 2.

    Critical Success The creature is frightened 4.'
  requirements: null
  success: The creature is frightened 1.
  traits:
  - aura
  - emotion
  - fear
  - mental
  trigger: null
- action_cost: Reaction
  critical_failure: null
  critical_success: null
  description: null
  effect: The dragon raises its wing, gaining a +2 circumstance bonus to AC against
    the triggering attack. If the dragon is flying, it descends 10 feet after the
    attack is complete.
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Wing Deflection
  range: null
  raw_description: '**Wing Deflection** [Reaction] **Trigger** The silver dragon is
    targeted with an attack. **Effect** The dragon raises its wing, gaining a +2 circumstance
    bonus to AC against the triggering attack. If the dragon is flying, it descends
    10 feet after the attack is complete.'
  requirements: null
  success: null
  traits: null
  trigger: The silver dragon is targeted with an attack.
description: 'Silver dragons are among the most chivalrous of all dragonkind; they
  wield frost and cold as weapons, can walk on clouds, and dwell high upon snowy mountain
  peaks or deep in steep, misty valleys. Although they typically make their lairs
  among the highlands, the pursuit of justice leads silver dragons to travel far and
  wide—often into the very heart of realms overrun by evil. These exemplars of righteousness
  are ceaseless in their determination to help the weak, spread honor, and stamp out
  evil.




  Silver dragons are sleek and sinuous. Their hides resemble nothing so much as a
  suit of gleaming armor, lending further credence to the popular myth that silver
  dragons are the paladins of dragonkind. The zeal with which they seek out, confront,
  and defeat evil is unsurpassed even among their metallic cousins, and they adhere
  to strict codes of honor usually passed down from parent to hatchling. On occasion,
  they instead learn these codes from trusted mentors, usually other silver dragons
  or gold dragons. As they age, they become even more dedicated to their codes, often
  adding new and even more restrictive clauses to the systems that guide their behavior.




  Silver dragons are incredibly altruistic and regularly consort with the citizens
  of goodly societies, of which they consider themselves protectors and guides. In
  addition to responding to evil threats, silver dragons work to prevent evil from
  taking root in the first place, and they ensure mortals under their care are well
  fed, educated, and treated with dignity. Although silver dragons can seem overzealous
  or even eager to join the fight against evil, they know that the best way to rid
  the world of corruption is to stamp out strife and disillusionment at their source,
  not to passively sit back and watch it grow into an unsolvable problem. Silver dragons
  can be vindictive, but they can also be forgiving; for evildoers who seek to atone
  for their sins and turn over a new leaf, the support of a silver dragon is both
  unwavering and invaluable.




  Many silver dragons are also drawn to religious endeavors, venerating deities such
  as Iomedae, Sarenrae, and other deities concerned with justice, virtue, and redemption.




  **__Recall Knowledge - Dragon__ (__Arcana__)**: DC 32'
hp: 295
hp_misc: null
immunities:
- cold
- paralyzed
- sleep
items: null
languages:
- Aquan
- Common
- Draconic
- Dwarven
level: 14
melee:
- action_cost: One Action
  damage:
    formula: 3d10+13
    type: piercing
  name: jaws
  plus_damage:
  - formula: 3d6
    type: cold
  to_hit: 29
  traits:
  - cold
  - magical
  - reach 15 feet
- action_cost: One Action
  damage:
    formula: 3d8+13
    type: slashing
  name: claw
  plus_damage: null
  to_hit: 29
  traits:
  - agile
  - magical
  - reach 10 feet
- action_cost: One Action
  damage:
    formula: 2d10+11
    type: bludgeoning
  name: tail
  plus_damage: null
  to_hit: 27
  traits:
  - magical
  - reach 20 feet
name: Adult Silver Dragon
perception: 26
proactive_abilities:
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The silver dragon breathes in one of two ways. The dragon can't use
    Breath Weapon again for 1d4 rounds.
  effect: null
  effects:
  - action_cost: None
    critical_failure: null
    critical_success: null
    description: The dragon breathes a cloud of frost in a 40-foot cone that deals
      15d6 cold damage (DC 35 basic Reflex save).
    effect: null
    effects: null
    failure: null
    frequency: null
    full_description: null
    generic_description: null
    name: Frost
    range: null
    raw_description: '**Frost** (__arcane__,__cold__,__evocation__); The dragon breathes
      a cloud of frost in a 40-foot cone that deals 15d6 cold damage (DC 35 basic
      Reflex save).'
    requirements: null
    success: null
    traits:
    - arcane
    - cold
    - evocation
    trigger: null
  - action_cost: None
    critical_failure: null
    critical_success: null
    description: The dragon breathes a blast of paralyzing gas. Each creature within
      a 40-foot cone must succeed at a DC 35 Fortitude save or be __slowed__ 2 for
      1 round (or __paralyzed__ for two rounds on a critical failure).
    effect: null
    effects: null
    failure: null
    frequency: null
    full_description: null
    generic_description: null
    name: Paralyzing Gas
    range: null
    raw_description: '**Paralyzing Gas** (__arcane__, __enchantment__,__incapacitation__);
      The dragon breathes a blast of paralyzing gas. Each creature within a 40-foot
      cone must succeed at a DC 35 Fortitude save or be __slowed__ 2 for 1 round (or
      __paralyzed__ for two rounds on a critical failure).'
    requirements: null
    success: null
    traits:
    - arcane
    - enchantment
    - incapacitation
    trigger: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Breath Weapon
  range: null
  raw_description: "**Breath Weapon ** [Two Actions]  The silver dragon breathes in\
    \ one of two ways. The dragon can't use Breath Weapon again for 1d4 rounds.\n\n\
    \  * **Frost** (__arcane__,__cold__,__evocation__); The dragon breathes a cloud\
    \ of frost in a 40-foot cone that deals 15d6 cold damage (DC 35 basic Reflex save).\n\
    \n  * **Paralyzing Gas** (__arcane__, __enchantment__,__incapacitation__); The\
    \ dragon breathes a blast of paralyzing gas. Each creature within a 40-foot cone\
    \ must succeed at a DC 35 Fortitude save or be __slowed__ 2 for 1 round (or __paralyzed__\
    \ for two rounds on a critical failure)."
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: The silver dragon can tread on clouds or fog as though on solid ground.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Cloud Walk
  range: null
  raw_description: '**Cloud Walk** The silver dragon can tread on clouds or fog as
    though on solid ground.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The silver dragon makes two claw Strikes and one tail Strike in any
    order.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Draconic Frenzy
  range: null
  raw_description: '**Draconic Frenzy** [Two Actions]  The silver dragon makes two
    claw Strikes and one tail Strike in any order.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: When the silver dragon scores a critical hit with a Strike, it recharges
    its Breath Weapon.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Draconic Momentum
  range: null
  raw_description: '**Draconic Momentum** When the silver dragon scores a critical
    hit with a Strike, it recharges its Breath Weapon.'
  requirements: null
  success: null
  traits: null
  trigger: null
ranged: null
rarity: Common
resistances:
- amount: 10
  type: fire
ritual_lists: null
saves:
  fort: 26
  fort_misc: null
  misc: +1 status to all saves vs. magic
  ref: 23
  ref_misc: null
  will: 28
  will_misc: null
sense_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: The silver dragon ignores the concealed condition from fog and clouds.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Fog Vision
  range: null
  raw_description: '**Fog Vision** The silver dragon ignores the concealed condition
    from fog and clouds.'
  requirements: null
  success: null
  traits: null
  trigger: null
senses:
- Perception +26
- darkvision
- fog vision
- scent (imprecise) 60 feet
size: Huge
skills:
- bonus: 21
  misc: null
  name: 'Acrobatics '
- bonus: 27
  misc: null
  name: 'Athletics '
- bonus: 25
  misc: null
  name: 'Diplomacy '
- bonus: 27
  misc: null
  name: 'Intimidation '
- bonus: 24
  misc: null
  name: 'Medicine '
- bonus: 24
  misc: null
  name: 'Religion '
- bonus: 20
  misc: null
  name: 'Society '
source:
- abbr: Bestiary
  page_start: 126
  page_stop: null
speed:
- amount: 50
  type: Land
- amount: 140
  type: fly
- amount: null
  type: cloud walk
spell_lists:
- dc: 33
  misc: null
  name: Arcane Innate Spells
  spell_groups:
  - heightened_level: null
    level: 1
    spells:
    - frequency: evil only
      name: detect alignment
      requirement: null
  to_hit: null
traits:
- LG
- Huge
- Cold
- Dragon
type: Creature
weaknesses: null

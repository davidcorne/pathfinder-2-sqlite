ability_mods:
  cha_mod: 3
  con_mod: 5
  dex_mod: 3
  int_mod: 4
  str_mod: 6
  wis_mod: 4
ac: 26
ac_special: null
alignment: CE
automatic_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: A brain collector has seven brain blisters on its back that it uses
    to house stolen brains. A brain collector without all seven blisters full is stupefied
    with a value equal to the number of empty blisters.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Brain Blisters
  range: null
  raw_description: '**Brain Blisters** A brain collector has seven brain blisters
    on its back that it uses to house stolen brains. A brain collector without all
    seven blisters full is stupefied with a value equal to the number of empty blisters.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: If a brain collector takes 30 damage from a critical hit or takes 25
    mental damage, it must succeed at a DC 26 save (Fortitude for critical damage
    or Will for mental damage) or one of its brain blisters is destroyed.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Brain Loss
  range: null
  raw_description: '**Brain Loss** If a brain collector takes 30 damage from a critical
    hit or takes 25 mental damage, it must succeed at a DC 26 save (Fortitude for
    critical damage or Will for mental damage) or one of its brain blisters is destroyed.'
  requirements: null
  success: null
  traits: null
  trigger: null
description: 'The grotesque brain collectors (or neh-thalggus, as they call themselves)
  originate from worlds far beyond the known solar system, and are part of a conglomeration
  of hostile aliens known collectively as the Dominion of the Black. Whether driven
  by their own schemes or directives from sinister overlords, brain collectors arrive
  in living starships to harvest the brains of intelligent creatures. These aberrations
  draw no nutrition from brains, instead storing them for analysis and as vessels
  for occult magical energies.




  A brain collector''s form evokes that of a tailless scorpion, but the pulsing brain-filled
  blisters that glisten along its back make them impossible to mistake for merely
  oversized arachnids. Baleful eyes glare from the joints on their legs, and the unsettling,
  intrusive whisper-thoughts they telepathically broadcast into the minds of those
  they seek to feed on can be interpreted as threats or promises alike.




  Brain collectors have very little empathy for the denizens of any world they visit,
  despite the fact that certain cults venerate them, or the Dominion they hail from,
  as if they were gods. To brain collectors, terrestrial creatures are simply resources
  for their magical needs and occult powers. They have little interest in worshipping
  gods or being worshipped themselves, yet they do practice strange forms of religion
  of their own, in which they consider the primordial forces of deep space as worthy
  of faith and fear.




  **__Recall Knowledge - Aberration__ (__Occultism__)**: DC 26'
hp: 140
hp_misc: null
immunities:
- confused
items: null
languages:
- Abyssal
- Aklo
- Common
- Draconic
- Protean
- Undercommon
- telepathy 100 feet
level: 8
melee:
- action_cost: One Action
  damage:
    formula: 2d12+6
    type: piercing
  name: Jaws
  plus_damage:
  - formula: null
    type: brain collector venom
  to_hit: 20
  traits: null
- action_cost: One Action
  damage:
    formula: 2d8+6
    type: slashing
  name: Claw
  plus_damage: null
  to_hit: 20
  traits:
  - agile
name: Brain Collector
perception: 18
proactive_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: __**Saving Throw**__ DC 26 Fortitude; **Maximum Duration** 6 rounds;
    **Stage 1** 1d6 poison damage and __enfeebled 1__ (1 round); **Stage 2** 1d6 poison
    damage, __enfeebled 1__, and __slowed 1__ (1 round); **Stage 3** 2d6 poison damage,
    __enfeebled 2__, and __slowed 1__ (1 round)
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Brain Collector Venom
  range: null
  raw_description: '**Brain Collector Venom** (__poison__) __**Saving Throw**__ DC
    26 Fortitude; **Maximum Duration** 6 rounds; **Stage 1** 1d6 poison damage and
    __enfeebled 1__ (1 round); **Stage 2** 1d6 poison damage, __enfeebled 1__, and
    __slowed 1__ (1 round); **Stage 3** 2d6 poison damage, __enfeebled 2__, and __slowed
    1__ (1 round)'
  requirements: null
  success: null
  traits:
  - poison
  trigger: null
- action_cost: One Action
  critical_failure: null
  critical_success: null
  description: The brain collector collects a brain of a creature that has been dead
    for no more than 1 minute. It can then use an Interact action to secure the brain
    in one of its empty brain blisters
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Collect Brain
  range: null
  raw_description: '**Collect Brain**   (__manipulate__) The brain collector collects
    a brain of a creature that has been dead for no more than 1 minute. It can then
    use an Interact action to secure the brain in one of its empty brain blisters'
  requirements: null
  success: null
  traits:
  - manipulate
  trigger: null
ranged: null
rarity: Uncommon
resistances:
- amount: null
  type: brain loss
ritual_lists: null
saves:
  fort: 15
  fort_misc: null
  misc: +1 status to all saves vs. magic
  ref: 13
  ref_misc: null
  will: 18
  will_misc: null
sense_abilities: null
senses:
- Perception +18
- darkvision
size: Large
skills:
- bonus: 17
  misc: null
  name: 'Acrobatics '
- bonus: 18
  misc: null
  name: 'Arcana '
- bonus: 16
  misc: null
  name: 'Athletics '
- bonus: 18
  misc: all subcategories
  name: 'Lore '
- bonus: 21
  misc: null
  name: 'Occultism '
- bonus: 17
  misc: null
  name: 'Stealth '
source:
- abbr: Bestiary
  page_start: 46
  page_stop: null
speed:
- amount: 25
  type: Land
- amount: 30
  type: fly
spell_lists:
- dc: 26
  misc: ''
  name: Occult Spontaneous Spells
  spell_groups:
  - heightened_level: null
    level: 4
    spells:
    - frequency: null
      name: confusion
      requirement: null
    - frequency: 2 slots
      name: phantasmal killer
      requirement: null
  - heightened_level: null
    level: 3
    spells:
    - frequency: null
      name: dispel magic
      requirement: null
    - frequency: null
      name: haste
      requirement: null
    - frequency: 3 slots
      name: paralyze
      requirement: null
  - heightened_level: null
    level: 2
    spells:
    - frequency: null
      name: humanoid form
      requirement: null
    - frequency: null
      name: invisibility
      requirement: null
    - frequency: null
      name: mirror image
      requirement: null
    - frequency: 4 slots
      name: paranoia
      requirement: null
  - heightened_level: null
    level: 1
    spells:
    - frequency: null
      name: mindlink
      requirement: null
    - frequency: null
      name: ray of enfeeblement
      requirement: null
    - frequency: null
      name: true strike
      requirement: null
    - frequency: 4 slots
      name: unseen servant
      requirement: null
  - heightened_level: 4
    level: 0
    spells:
    - frequency: null
      name: dancing lights
      requirement: null
    - frequency: null
      name: detect magic
      requirement: null
    - frequency: null
      name: mage hand
      requirement: null
    - frequency: null
      name: prestidigitation
      requirement: null
  to_hit: 18
traits:
- Uncommon
- CE
- Large
- Aberration
type: Creature
weaknesses: null

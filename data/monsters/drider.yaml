ability_mods:
  cha_mod: 4
  con_mod: 3
  dex_mod: 3
  int_mod: 2
  str_mod: 4
  wis_mod: 3
ac: 24
ac_special: null
alignment: CE
automatic_abilities: null
description: 'The first fleshwarping process mastered by the drow remains both their
  most successful and most infamous: the drider. Fusing the body of a drow with that
  of a giant spider, driders are a sexually dimorphic fleshwarp—the only fleshwarp
  known to be able to produce young. While female driders have the upper torsos of
  elegant drow women with mouths featuring sharp poisoned fangs, male driders have
  hideous, mutated countenances that further blend the humanoid form with that of
  a spider; the difference in appearance is perhaps a reflection on the matriarchal
  nature of drow society. In combat, all driders are equally dangerous.




  Although the fleshwarping process was originally intended to be a punishment for
  drow who fell from grace, the significant powers and strengths gained from the horrifying
  transformation are attractive to some drow, and an increasing number of lower-ranking
  citizens in drow cities volunteer themselves for the painful procedure. In return
  for mandatory time (typically measured in decades) serving as a guardian, soldier,
  or other public servant, such volunteer driders are later allowed to lead their
  own lives, typically in caverns located in a city''s periphery. The painful truth
  is that for most driders, they do not live long enough to see this reward, for the
  drow have a penchant for working their fleshwarped slaves to death or deploying
  them in deadly war missions in which survival is a only a remote possibility.




  Most driders use arcane magic, but driders who use divine magic are well known,
  while a rare few use occult or primal magic. A drider''s innate spells remain the
  same regardless of the magical tradition, but those who use different types of magic
  as prepared spells may well have entirely different spells prepared than the drider
  presented below.




  **__Recall Knowledge - Aberration__ (__Occultism__)**: DC 22'
hp: 95
hp_misc: null
immunities:
- sleep
items:
- +1 composite longbow (20 arrows)
- glaive
languages:
- Elven
- Undercommon
level: 6
melee:
- action_cost: One Action
  damage:
    formula: 1d8+10
    type: slashing
  name: glaive
  plus_damage: null
  to_hit: 16
  traits:
  - deadly 1d8
  - forceful
  - reach 10 feet
- action_cost: One Action
  damage:
    formula: 1d6+10
    type: piercing
  name: fangs
  plus_damage:
  - formula: null
    type: drider venom
  to_hit: 16
  traits: null
name: Drider
perception: 13
proactive_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: '**Saving Throw** DC 23 Fortitude; **Maximum Duration** 6 rounds; **Stage
    1** 1d8 poison damage and __enfeebled 1__ (1 round)'
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Drider Venom
  range: null
  raw_description: '**Drider Venom** (__poison__) **Saving Throw** DC 23 Fortitude;
    **Maximum Duration** 6 rounds; **Stage 1** 1d8 poison damage and __enfeebled 1__
    (1 round)'
  requirements: null
  success: null
  traits:
  - poison
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: A creature hit by a drider's web attack is __immobilized__ and stuck
    to the nearest surface (Escape DC 21).
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Web Trap
  range: null
  raw_description: '**Web Trap** A creature hit by a drider''s web attack is __immobilized__
    and stuck to the nearest surface (Escape DC 21).'
  requirements: null
  success: null
  traits: null
  trigger: null
ranged:
- action_cost: One Action
  damage:
    formula: 1d8+8
    type: piercing
  name: composite longbow
  plus_damage: null
  to_hit: 16
  traits:
  - deadly d10
  - magical
  - propulsive
  - range increment 100 feet
  - reload 0
  - volley 50 feet
- action_cost: One Action
  damage: null
  name: web
  plus_damage: null
  to_hit: 15
  traits:
  - range increment 30 feet
rarity: Common
resistances: null
ritual_lists: null
saves:
  fort: 13
  fort_misc: null
  misc: +1 status to all saves vs. magic
  ref: 13
  ref_misc: null
  will: 15
  will_misc: null
sense_abilities: null
senses:
- Perception +13
- darkvision
size: Large
skills:
- bonus: 14
  misc: null
  name: 'Arcana '
- bonus: 12
  misc: null
  name: 'Athletics '
- bonus: 14
  misc: null
  name: 'Intimidation '
- bonus: 13
  misc: null
  name: 'Religion '
- bonus: 15
  misc: null
  name: 'Stealth '
source:
- abbr: Bestiary
  page_start: 159
  page_stop: null
speed:
- amount: 30
  type: Land
- amount: 20
  type: climb
spell_lists:
- dc: 20
  misc: null
  name: Arcane Innate Spells
  spell_groups:
  - heightened_level: null
    level: 4
    spells:
    - frequency: null
      name: clairvoyance
      requirement: null
    - frequency: null
      name: suggestion
      requirement: null
  - heightened_level: null
    level: 3
    spells:
    - frequency: null
      name: clairaudience
      requirement: null
    - frequency: null
      name: dispel magic
      requirement: null
    - frequency: null
      name: levitate
      requirement: null
  - heightened_level: null
    level: 2
    spells:
    - frequency: at will
      name: darkness
      requirement: null
    - frequency: at will
      name: faerie fire
      requirement: null
  - heightened_level: 3
    level: 0
    spells:
    - frequency: null
      name: dancing lights
      requirement: null
    - frequency: null
      name: detect magic
      requirement: null
  to_hit: null
- dc: 24
  misc: ''
  name: Arcane Prepared Spells
  spell_groups:
  - heightened_level: null
    level: 3
    spells:
    - frequency: null
      name: fireball
      requirement: null
  - heightened_level: null
    level: 2
    spells:
    - frequency: null
      name: acid arrow
      requirement: null
    - frequency: null
      name: invisibility
      requirement: null
  - heightened_level: null
    level: 1
    spells:
    - frequency: x2
      name: magic missile
      requirement: null
    - frequency: null
      name: ray of enfeeblement
      requirement: null
  - heightened_level: 3
    level: 0
    spells:
    - frequency: null
      name: ghost sound
      requirement: null
    - frequency: null
      name: mage hand
      requirement: null
    - frequency: null
      name: ray of frost
      requirement: null
  to_hit: 17
traits:
- CE
- Large
- Aberration
type: Creature
weaknesses: null
